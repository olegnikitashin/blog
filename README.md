# Portfolio App

### Description
----
This application represents a simple Portfolio website with Ruby on Rails.

### Main features
----
It offers the following features:
  - Owner can Create/Edit/Delete Articles;
  - Owner can Create/Edit/Delete Projects;
  - Guest can View Articles/Progects;
  - Guest can Comment Articles with Disqus;
  - Guest can contact the Owner with contact form;
  - Articles/Projects support markdown & syntax highlighting;

### Usage
----
Please try the latest version following [this](https://on-olegnikitashin.herokuapp.com) link!
